﻿using System;
using System.Collections;
using Game.Scripts.Utils;
using UnityEngine;
using UnityEngine.SceneManagement;
/*
namespace Game.Scripts.Scenes
{
    public class SceneLoader : Singleton<SceneLoader>
    {

        [SerializeField] private CanvasGroup _canvasGroup;
        
        
        private bool Enable
        {
            set
            {
                if (value)
                {
                    _canvasGroup.alpha = 1f;
                    _canvasGroup.interactable = true;
                    _canvasGroup.blocksRaycasts = true;
                }
                else
                {
                    _canvasGroup.alpha = 0f;
                    _canvasGroup.interactable = false;
                    _canvasGroup.blocksRaycasts = false;
                }
            }
        }
        public void LoadScene(string sceneName)
        {
            StartCoroutine(SceneLoadCoroutine(sceneName));
        }

        private IEnumerator SceneLoadCoroutine(string sceneName)
        {
            Enable = true;
            AsyncOperation operation = SceneManager.LoadSceneAsync(sceneName);
            while (!operation.isDone)
            {
                yield return null;
            }
            Enable = false;
        }

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
        private static void OnAfterSceneLoad()
        {
            InitLoader();
        }

        private static void InitLoader()
        {
            SceneManager.LoadScene("PreLoader", LoadSceneMode.Additive);
        }

        protected override void Awake()
        {
            base.Awake();

            Enable = false;
            DontDestroyOnLoad(gameObject);
        }

        private void Start()
        {
            Enable = false;
        }
    }
}*/