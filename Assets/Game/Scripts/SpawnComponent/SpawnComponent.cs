﻿using System;
using System.Collections;
using Game.Scripts.Utils;
using UnityEngine;

namespace Game.Scripts.SpawnComponent
{
    public class SpawnComponent : MonoBehaviour
    {
        [SerializeField] private Transform _target;
        [SerializeField] private GameObject _prefab;
        [SerializeField] private float _amountZombie;
        [SerializeField] private float _delay;
        private Coroutine _coroutine;


        private void Start()
        {
            SpawnZombie();
        }

        [ContextMenu("Spawn")]
        public void SpawnZombie()
        {
            if(_coroutine != null)
                StopCoroutine(_coroutine);
            _coroutine = StartCoroutine(SpawnZombies());
        }

        private IEnumerator SpawnZombies()
        {
            for (int i = 0; i < _amountZombie; i++)
            {
                var instance =SpawnUtils.Spawn(_prefab, _target.position);
                yield return new WaitForSeconds(_delay);
            }

            _coroutine = null;
        }
    }
}