﻿using System;
using Game.Scripts.Utils;
using Game.Scripts.Utils.Disposables;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Game.Scripts.Input
{
    public class InputManager : Singleton<InputManager>
    {
        private JoyStickControls _joyStickControls;
        private bool _isTouchToScreen = false;

        public bool IsTouchToScreen
        {
            get => _isTouchToScreen;
            set => _isTouchToScreen = value;
        }


        public Vector2 TouchPosition => _joyStickControls.Touch.TouchPosition.ReadValue<Vector2>();
        public Vector2 StartTouchPosition { get; private set; }

        public event Action OnStartJoyStickTouch;
        public event Action OnJoyEndStickTouch;

        protected override void Awake()
        {
            base.Awake();
            _joyStickControls = new JoyStickControls();
        }

        private void OnEnable()
        {
            _joyStickControls.Enable();
        }

        private void OnDisable()
        {
            _joyStickControls.Disable();
        }

        private void Start()
        {
            _joyStickControls.Touch.TouchPress.started += ctx => StartTouch(ctx);
            _joyStickControls.Touch.TouchPress.canceled += ctx => EndTouch(ctx);
        }
        
        
        public IDisposable SubscribeOnStartTouch(Action call)
        {
            OnStartJoyStickTouch += call;
            return new ActionDisposable(() => OnStartJoyStickTouch -= call);
        }
        public IDisposable SubscribeOnEndTouch(Action call)
        {
            OnJoyEndStickTouch += call;
            return new ActionDisposable(() => OnJoyEndStickTouch -= call);
        }
        
        private void StartTouch(InputAction.CallbackContext ctx)
        {
            _isTouchToScreen = true;
            StartTouchPosition = TouchPosition;
            OnStartJoyStickTouch?.Invoke();
        }
        private void EndTouch(InputAction.CallbackContext ctx)
        {
            _isTouchToScreen = false;
             OnJoyEndStickTouch?.Invoke();
        }
    }
}