﻿using Game.Scripts.Gameplay.Model;
using PixelCrew.Components;
using UnityEngine;

namespace Game.Scripts.Gameplay.GoBased
{
    public class DestroyObjectComponent : MonoBehaviour
    {
        [SerializeField] private GameObject _objectToDestroy;
        [SerializeField] private RestoreStateComponent _state;
        public void DestroyObject()
        {
            Destroy(_objectToDestroy);
            if (_state != null)
                GameSession.Instance.StoreState(_state.Id);
        }
    }
}