﻿using UnityEngine;

namespace Game.Scripts.Components.ColliderBased
{
    public class RaycastCheck : LayerCheck
    {
        [SerializeField] private float distance;
        [SerializeField] private LayerMask _object;

        private Transform m_Transform;

        private void Awake()
        {
            m_Transform = GetComponent<Transform>();
        }

        private void FixedUpdate()
        {
            Ray ray = new Ray(m_Transform.position, m_Transform.forward);
            Debug.DrawRay(m_Transform.position, m_Transform.forward * distance);

            if (Physics.Raycast(ray, out RaycastHit hitInfo, distance, _object))
            {
                
                Debug.Log(hitInfo.collider.name);
            }
        }
        
        
    }
}