﻿using UnityEngine;

namespace Game.Scripts.Components.ColliderBased
{
    public class ColliderCheck: LayerCheck
    {
        private Collider _collider;
        
        private void Awake()
        {
            _collider = GetComponent<Collider>();
        }

        private void OnTriggerStay(Collider other)
        {
            RaycastHit hitInfo;
            _isTouchingLayer = _collider.Raycast(default, out hitInfo, 0);
        }

        private void OnTriggerExit(Collider other)
        {
            RaycastHit hitInfo;
            _isTouchingLayer = _collider.Raycast(default, out hitInfo, 0);
        }
    }
}