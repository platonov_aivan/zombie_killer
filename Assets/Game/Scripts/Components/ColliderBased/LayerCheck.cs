﻿using UnityEngine;

namespace Game.Scripts.Components.ColliderBased
{
    public class LayerCheck: MonoBehaviour
    {
        [SerializeField] protected LayerMask _layer;
        [SerializeField] protected bool _isTouchingLayer;
        public bool IsTouchingLayer
        {
            get => _isTouchingLayer;
            set => _isTouchingLayer = value;
        }
    }
}