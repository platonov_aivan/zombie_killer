﻿using System;
using Game.Scripts.Utils;
using Game.Scripts.Utils.Disposables;
using UnityEngine;
using UnityEngine.Events;

namespace Game.Scripts.Components.Health
{
    public class HealthComponent : MonoBehaviour
    {
        public event Action OnDie;

        public delegate void LifeBarHeal(int _healy);

        public event LifeBarHeal OnLifeBarHeals;
        
        [SerializeField] private int _health;
        [SerializeField] public UnityEvent _onDamage;
        [SerializeField] public UnityEvent _onDie;
        [SerializeField] private UnityEvent _onHeal;
        [SerializeField] public HealthChangeEvent _onChange;
        private Lock _immune = new Lock();

        public int Health => _health;
        public Lock Immune => _immune;

        public IDisposable SubscribeOnDie(Action call)
        {
            OnDie += call;
            return new ActionDisposable(() => OnDie -= call);
        }
        public IDisposable SubscribeOnChange(LifeBarHeal call)
        {
            OnLifeBarHeals += call;
            return new ActionDisposable(() => OnLifeBarHeals -= call);
        }


        public void ChangeHealth(int deltaHealth)
        {
            if(Immune.IsLocked) return;
            if (_health <= 0) return;
            
            _health += deltaHealth;
            _onChange?.Invoke(_health);
            OnLifeBarHeals?.Invoke(_health);
            
            if (deltaHealth <0)
            {
                _onDamage?.Invoke();

            } else if (deltaHealth>0)
            {
                _onHeal?.Invoke();
            } 
            if (_health <= 0)
            {
                _onDie?.Invoke();
                OnDie?.Invoke();
                _health = 0;
            }
        }


#if UNITY_EDITOR
        [ContextMenu("Update Health")]
        private void UpdateHealth()
        {
            _onChange?.Invoke(_health);
            OnLifeBarHeals?.Invoke(_health);
        }
        [ContextMenu("Kill")]
        private void Kill()
        {
            ChangeHealth(_health * -1);
        }
#endif

        public void SetHealth(int health)
        {
            _health = health;
        }

        private void OnDestroy()
        {
            _onDie.RemoveAllListeners();
        }

        [Serializable]
        public class HealthChangeEvent : UnityEvent<int>
        {
            
        }
    }
    
}

