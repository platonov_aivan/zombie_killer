﻿using UnityEngine;

namespace Game.Scripts.Colliders.Triggers
{
	public class EnterTrigger : BasePhysicsInteract
	{
		private void OnTriggerEnter(Collider other)
		{
			PhysicsInteract(other.gameObject);
		}
	}
}