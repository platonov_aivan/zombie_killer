﻿using Game.Scripts.Colliders;
using UnityEngine;
namespace Game.Scripts.Gameplay.Colliders.Triggers
{
	public class StayTrigger : BasePhysicsInteract
	{
		private void OnTriggerStay(Collider other)
		{
			PhysicsInteract(other.gameObject);
		}
	}
}