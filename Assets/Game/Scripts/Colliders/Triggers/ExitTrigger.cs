﻿using Game.Scripts.Colliders;
using UnityEngine;
namespace Game.Scripts.Gameplay.Colliders.Triggers
{
	public class ExitTrigger : BasePhysicsInteract
	{
		private void OnTriggerExit(Collider other)
		{
			PhysicsInteract(other.gameObject);
		}
	}
}