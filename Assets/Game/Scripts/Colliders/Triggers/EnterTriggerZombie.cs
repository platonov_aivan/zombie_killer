﻿using UnityEngine;

namespace Game.Scripts.Colliders.Triggers
{
	public class EnterTriggerZombie : BasePhysicsInteract
	{
		private void OnTriggerEnter(Collider other)
		{
			PhysicsInteract(other.gameObject);
		}
	}
}