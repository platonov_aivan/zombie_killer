﻿using System;
using Game.Scripts.Utils;
using Game.Scripts.Utils.Disposables;
using UnityEngine;

namespace Game.Scripts.Colliders
{
	public abstract class BasePhysicsInteract : MonoBehaviour
	{
		public event Action OnInteract;
		public event Action<GameObject> OnInteractGameObject;

		[SerializeField] private string _tag;
		[SerializeField] private LayerMask _layer = ~0;
		
		public IDisposable SubscribeOnInteract(Action call)
		{
			OnInteract += call;
			return new ActionDisposable(() => OnInteract -= call);
		}
		public IDisposable SubscribeOnInteractGameObject(Action<GameObject> call)
		{
			OnInteractGameObject += call;
			return new ActionDisposable(() => OnInteractGameObject -= call);
		}

		protected void PhysicsInteract(GameObject obj)
		{
			if (!obj.gameObject.IsInLayer(_layer)) return;
			if (!string.IsNullOrEmpty(_tag) && !string.IsNullOrEmpty(obj.gameObject.tag) && !obj.gameObject.CompareTag(_tag)) return;
			OnInteract?.Invoke();
			OnInteractGameObject?.Invoke(obj.gameObject);
		}
	}
}