﻿using Game.Scripts.Colliders;
using UnityEngine;
namespace Game.Scripts.Gameplay.Colliders.Collisions
{
	public class StayCollision : BasePhysicsInteract
	{
		private void OnCollisionStay(Collision collisionInfo)
		{
			PhysicsInteract(collisionInfo.gameObject);
		}
	}
}