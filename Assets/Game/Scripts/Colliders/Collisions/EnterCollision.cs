﻿using Game.Scripts.Colliders;
using UnityEngine;
namespace Game.Scripts.Gameplay.Colliders.Collisions
{
	public class EnterCollision : BasePhysicsInteract
	{
		private void OnCollisionEnter(Collision collision)
		{
			PhysicsInteract(collision.gameObject);
		}
	}
}