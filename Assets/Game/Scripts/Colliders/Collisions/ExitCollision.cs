﻿using Game.Scripts.Colliders;
using UnityEngine;
namespace Game.Scripts.Gameplay.Colliders.Collisions
{
	public class ExitCollision : BasePhysicsInteract
	{
		private void OnCollisionExit(Collision other)
		{
			PhysicsInteract(other.gameObject);
		}
	}
}