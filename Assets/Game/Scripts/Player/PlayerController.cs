﻿using System;
using System.Collections.Generic;
using System.Linq;
using Game.Scripts.CheckCircle;
using Game.Scripts.Colliders.Triggers;
using Game.Scripts.Components.Health;
using Game.Scripts.Enemy;
using Game.Scripts.Gameplay.Colliders.Triggers;
using Game.Scripts.Utils;
using Game.Scripts.Utils.ObjectPool;
using UnityEngine;

namespace Game.Scripts.Player
{
    public class PlayerController : MonoBehaviour
    {
        [Header("Player")]
        [SerializeField] private Transform _pelvisPlayer;
        [SerializeField] private Animator _animator;
        [SerializeField] private HealthComponent _health;
        [SerializeField] private CheckCircleOverlap _checkZombie;
        [SerializeField] private PlayerMovement _playerMovement;
        [Space]
        [Header("Projectile")]
        [SerializeField] private GameObject _projectilePrefab;
        [SerializeField] private Transform _projectileSpawnPoint;

        private Collider _currentZombie;

        

        

        private Transform CachedTransform => _cachedTransform == null ? _cachedTransform = transform : _cachedTransform;
        private Transform _cachedTransform;
        private static readonly int Shooting = Animator.StringToHash("is-shooting");

        private bool _zoombieALive;
        private Vector3 _currentVector;

        public bool ZoombieALive
        {
            get => _zoombieALive;
            set => _zoombieALive = value;
        }
        private void Start()
        {
            _health.OnDie += OnDie;
        }

        public void OnShoot()
        {
            var bullet = Pool.Instance.Get(_projectilePrefab, _projectileSpawnPoint.position);
            var shootingComponent = bullet.GetComponent<ShootingComponent>();
            shootingComponent._shootTarget = _currentZombie.transform;
            shootingComponent.Initialize();
        }
        private void Update()
        {
            OnSortingZombieDistance();
        }
        private void OnSortingZombieDistance()
        {
            var colliderArray = _checkZombie.Check();
            var currentMinDistance = 100f;

            foreach (var colliderZombie in colliderArray)
            {
                if (colliderZombie == null) continue;
                var zombieTransform = colliderZombie.transform;
                var zombieVector = zombieTransform.position - _pelvisPlayer.transform.position;
                var zombieDistance = zombieVector.magnitude;
               
                if (currentMinDistance > zombieDistance && colliderZombie.GetComponent<EnemyController>().IsALive)
                {
                    currentMinDistance = zombieDistance;
                    _currentZombie = colliderZombie;
                    _currentVector = zombieVector;
                }
            }
            if (!_zoombieALive)
            {
                _animator.SetBool(Shooting, false);
                _playerMovement._directionZombie = Vector3.zero;
                return;
            } 
            if (_zoombieALive)
            {
                if (_currentZombie != null)
                {
                    _playerMovement._directionZombie = _currentVector.normalized;
                    ShootToTarget(_currentZombie);
                }  
            }
        }
        
        private void OnDie()
        {
            _animator.enabled = false;
        }
        
        private void ShootToTarget(Collider shootTarget)
        {
            if (shootTarget != null)
            {
                _animator.SetBool(Shooting, true);
            }
        }
        
        private void OnDestroy()
        {
            _health.OnDie -= OnDie;
            
        }
    }
}