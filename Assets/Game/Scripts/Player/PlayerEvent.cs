﻿using UnityEngine;

namespace Game.Scripts.Player
{
    public class PlayerEvent : MonoBehaviour
    {
        [SerializeField] private PlayerController _player;
        
        public void OnShoot()
        {
            _player.OnShoot();
        }
    }
}