﻿using Game.Scripts.Colliders.Triggers;
using Game.Scripts.Components.Health;
using Game.Scripts.Gameplay.Colliders.Triggers;
using Game.Scripts.Utils;
using Game.Scripts.Utils.ObjectPool;
using UnityEngine;

namespace Game.Scripts.Player
{
    public class ShootingComponent : MonoBehaviour
    {
        [Header("Components")]
        [SerializeField] private EnterTrigger _trigger;
        [SerializeField] private PoolItem _poolItem;
        [SerializeField] private Collider _collider;
        [Space] 
        [Header("Settings")] 
        [SerializeField] public float _speed;
        [SerializeField] private int _damage;
        [SerializeField] private float _lifeTimeProjectile;
        [Header("LayerMask")] 
        [SerializeField] private LayerMask _terrain;
        [SerializeField] private LayerMask _enemyRagdoll;
        
       /* [Header("Particle")]
        [SerializeField] private Transform _happyParticleSpawnPoint;
        [SerializeField] private GameObject _happyParticlePrefab;*/
        
        
        private Transform CachedTransform => _cachedTransform == null ? _cachedTransform = transform : _cachedTransform;
        private Transform _cachedTransform;

        
        
        private float _timeSpawn;

        private Rigidbody _rigidbody;

        public Transform _shootTarget;
        private Quaternion _updatedRotation;
        private float _startRotationSpeed;
        private Vector3 _avgDistance;

        private void Awake()
        {
            
            _rigidbody = GetComponent<Rigidbody>();
            _trigger.OnInteractGameObject += OnInteract;
            _poolItem.OnRestart += OnRestart;
        }
        private void OnRestart()
        {
            _collider.enabled = true;
        }

        private void OnInteract(GameObject obj)
        {
            if (obj.gameObject.IsInLayer(_enemyRagdoll))
            {
                var enemyHealth = obj.GetComponentInParent<HealthComponent>();
                enemyHealth.ChangeHealth(_damage);
                _collider.enabled = false;
                
               // Pool.Instance.Get(_happyParticlePrefab, CachedTransform.position);
                
                _poolItem.Release();
            }
            else if (obj.gameObject.IsInLayer(_terrain))
            {
                _collider.enabled = false;
                _poolItem.Release();
            }
        }

        private void FixedUpdate()
        {
            _rigidbody.MovePosition(_rigidbody.position + _avgDistance);
            
            if (_timeSpawn + _lifeTimeProjectile < Time.time)
            {
                _poolItem.Release();
            }
        }

        public void Initialize()
        {
            if (Locator.Instance.Player.ZoombieALive)
            {
                var zombieDistance =_shootTarget.position - transform.position;
                zombieDistance = zombieDistance.normalized;
                zombieDistance *= _speed * Time.fixedDeltaTime;
                _avgDistance = zombieDistance;
                _timeSpawn = Time.time; 
            }
        }

        private void OnDestroy()
        {
            _trigger.OnInteractGameObject -= OnInteract;
            _poolItem.OnRestart -= OnRestart;
        }
    }
}