﻿using System;
using Game.Scripts.Input;
using Game.Scripts.Utils;
using Game.Scripts.Utils.Disposables;
using UnityEngine;

namespace Game.Scripts.Player
{
    public class PlayerMovement : MonoBehaviour
    {
        [SerializeField] private float _speed = 2f;
        [SerializeField]private float _rotationSpeed = 2f;
        
        [SerializeField] private Animator _animator;

        private InputManager _inputManager;
        private Rigidbody _rigidbody;
        public Vector3 _directionZombie = Vector3.zero;
        
        
        
        
        private CompositeDisposable _trash = new CompositeDisposable();
        
        private static readonly int Movement = Animator.StringToHash("movement");
        private static readonly int Xdirection = Animator.StringToHash("x direction");
        private static readonly int Ydirection = Animator.StringToHash("y direction");
        private Quaternion _updatedRotation;
        
        private Transform CachedTransform => _cachedTransform == null ? _cachedTransform = transform : _cachedTransform;
        private Transform _cachedTransform;

        private void Awake()
        {
            _inputManager = InputManager.Instance;
            _rigidbody = GetComponent<Rigidbody>();
        }

        private void Start()
        {
            _trash.Retain(_inputManager.SubscribeOnStartTouch(MoveJoyStick));
            _trash.Retain(_inputManager.SubscribeOnEndTouch(StopJoyStick));
        }

        private void MoveJoyStick()
        {
            
        }
        private void StopJoyStick()
        {
            
        }
        private void FixedUpdate()
        {
            if (_inputManager.IsTouchToScreen &&  _directionZombie != Vector3.zero)
            {
                MoveAndShoot();
                var vectorRotation = _directionZombie;
                vectorRotation.y = 0f;
                //var rotation = Quaternion.LookRotation(vectorRotation);
               // _rigidbody.MoveRotation(rotation);
               MoveRotation(vectorRotation);
            }
            else if(_inputManager.IsTouchToScreen  &&  _directionZombie == Vector3.zero)
            {
                Move();
            }
            else if(_inputManager.IsTouchToScreen == false && _directionZombie != Vector3.zero)
            {
                StopAndShoot();
            }
            else if(_inputManager.IsTouchToScreen == false && _directionZombie == Vector3.zero)
            {
                Stop();
            }
        }

        private void MoveAndShoot()
        {
            var direction3D = CreateMoveVector();
            var dir = CreateDirection();
            _rigidbody.MovePosition(direction3D + _rigidbody.position);
            _rigidbody.angularVelocity = Vector3.zero;
            var dirZombieVec2 = new Vector2(_directionZombie.x, _directionZombie.z);
            MoveAnimation(ChangeBasisVector2(dir, dirZombieVec2));
        }
        private void Move()
        {
            var direction3D = CreateMoveVector();
            _rigidbody.MovePosition(direction3D + _rigidbody.position);
            _rigidbody.angularVelocity = Vector3.zero;
            MoveAnimation(new Vector2(0,1));
            MoveRotation(direction3D);
        }

        private Vector3 CreateMoveVector()
        {
            var dir = CreateDirection();
            return  new Vector3(dir.x, 0f, dir.y) * _speed * Time.fixedDeltaTime;
        }
        
        private Vector2 CreateDirection()
        {
            var currentTouchPos = _inputManager.TouchPosition;
            var startTouchPos = _inputManager.StartTouchPosition;
            if (currentTouchPos == startTouchPos)
            {
                return Vector2.one;
            }
            var dir = (currentTouchPos - startTouchPos).normalized;
            return dir;
        }

        private void MoveAnimation(Vector2 animVector)
        {
            _animator.SetFloat(Xdirection,animVector.x);
            _animator.SetFloat(Ydirection,animVector.y);
        }

        private void MoveRotation(Vector3 eulerAngles)
        {
            var rotationQuaternion = eulerAngles;
            rotationQuaternion.y = 0f;
            _updatedRotation = Quaternion.Euler(0f, Quaternion.LookRotation(rotationQuaternion).eulerAngles.y, 0f);
            _rigidbody.MoveRotation(Quaternion.Slerp(CachedTransform.rotation, _updatedRotation, _rotationSpeed * Time.fixedDeltaTime));
            _rigidbody.angularVelocity = Vector3.zero;
        }
        

        private void StopAndShoot()
        {
            _rigidbody.angularVelocity = Vector3.zero;
            _animator.SetFloat(Xdirection, 0);
            _animator.SetFloat(Ydirection, 0);
            var vectorRotation = _directionZombie;
            vectorRotation.y = 0f;
            MoveRotation(vectorRotation);
        }
        
        private void Stop()
        {
            _rigidbody.angularVelocity = Vector3.zero;
            _animator.SetFloat(Xdirection, 0);
            _animator.SetFloat(Ydirection, 0);
        }


        private static Vector2 ChangeBasisVector2(Vector2 source, Vector2 basis)
        {
            var detA = (basis.x * (-1 * basis.x)) - (basis.y * basis.y);

            var obr11 = (-1 / detA) * basis.x;
            var obr12 = (-1 / detA) * basis.y;
            var obr21 = (-1 / detA) * basis.y;
            var obr22 = (1 / detA) * basis.x;

            var y = obr11 * source.x + obr12 * source.y;
            var x = obr21 * source.x + obr22 * source.y;

            return new Vector2(x, y);
        }
        private void OnDestroy()
        {
            _trash.Dispose();
        }
    }
}