﻿using System;
using Game.Scripts.Input;
using Game.Scripts.Utils.Disposables;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Scripts.UI
{
    public class StickWidget : MonoBehaviour
    {
        [SerializeField] private RectTransform _stickBase;
        [SerializeField] private RectTransform _stick;

        private CompositeDisposable _trash = new CompositeDisposable();
        
        private void Start()
        {
            _trash.Retain(InputManager.Instance.SubscribeOnStartTouch(OnStartTouch));
            _trash.Retain(InputManager.Instance.SubscribeOnEndTouch(OnStopTouch));
        }

        private void Update()
        {
            if (InputManager.Instance.IsTouchToScreen == true)
            {
                var inputDirection = InputManager.Instance.TouchPosition - InputManager.Instance.StartTouchPosition;
                var stickBigWidth = _stickBase.sizeDelta.x;
                if (inputDirection.magnitude >= stickBigWidth / 2)
                {
                    var dir = (inputDirection).normalized;
                    _stick.localPosition = dir * (stickBigWidth / 2);
                }
                else
                {
                    var dir = inputDirection;
                    _stick.localPosition = dir;
                }
            }
        }

        private void OnStartTouch()
        {
            _stickBase.gameObject.SetActive(true);
            var startTouchPosition = InputManager.Instance.TouchPosition;
            _stickBase.position = startTouchPosition;
        }
        private void OnStopTouch()
        {
            _stickBase.gameObject.SetActive(false);
        }

        private void OnDestroy()
        {
            _trash.Dispose();
        }
    }
}