﻿using System;
using Game.Scripts.Gameplay.Model.Data.Properties;
using Game.Scripts.Gameplay.Model.Data.Properties.Persistent;
using UnityEngine;

namespace Game.Scripts.Gameplay.Model.Data
{
    [Serializable]
    public class PlayerData
    {
        public IntPersistentProperty ProgressSave;
        public IntProperty CurrentLevel;
        public IntProperty Hp;
        public void InitPlayerData()
        {
            ProgressSave = new IntPersistentProperty(0, "ProgressSave");
            CurrentLevel = new IntProperty();
            Hp = new IntProperty();
        }
    }
}