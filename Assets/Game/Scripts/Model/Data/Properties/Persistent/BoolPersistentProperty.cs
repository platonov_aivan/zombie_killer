﻿using System;
using UnityEngine;
namespace Game.Scripts.Gameplay.Model.Data.Properties.Persistent
{
	[Serializable]
	public class BoolPersistentProperty : PrefsPersistentProperty<bool>
	{

		public BoolPersistentProperty(bool defaultValue, string key) : base(defaultValue, key)
		{
			Init();
		}
		
		protected override void Write(bool value)
		{
			PlayerPrefs.SetInt(Key, value ? 1 : 0);
			base.Write(value);
		}
		protected override bool Read(bool defaultValue)
		{
			var value = PlayerPrefs.GetInt(Key, defaultValue ? 1 : 0);

			if (value == 1)
				return true;
			else if (value == 0)
				return false;
			
			return defaultValue;
		}
	}
}