﻿using Game.Scripts.Utils;
using UnityEngine;
namespace Game.Scripts.Gameplay.Model.Data.Properties.Persistent
{
	public class PersistentPropertySaver : Singleton<PersistentPropertySaver>
	{
		[SerializeField] private float _saveDelay = 5f;
		private bool _isSomeoneChanged;
		private float _timeLastSave;


		public void SomeonePropertyChanged()
		{
			_isSomeoneChanged = true;
		}

		private void Update()
		{
			if(!_isSomeoneChanged) return;
			if(Time.time < _timeLastSave + _saveDelay) return;
			
			PlayerPrefs.Save();
			
			_isSomeoneChanged = false;
			_timeLastSave = Time.time;
			
		}

		protected override void Awake()
		{
			base.Awake();
			DontDestroyOnLoad(gameObject);
		}
	}
}