﻿using System;
using UnityEngine;
namespace Game.Scripts.Gameplay.Model.Data.Properties.Persistent
{
    [Serializable]
    public class IntPersistentProperty : PrefsPersistentProperty<int>
    {
        public IntPersistentProperty(int defaultValue, string key) : base(defaultValue, key)
        {
            Init();
        }

        protected override void Write(int value)
        {
            PlayerPrefs.SetInt(Key, value);
            base.Write(value);
        }

        protected override int Read(int defaultValue)
        {
            return PlayerPrefs.GetInt(Key, defaultValue);
        }
    }
}