﻿using UnityEngine;
namespace Game.Scripts.Gameplay.Model.Data.Properties.Persistent
{
	public class StringPersistentProperty : PrefsPersistentProperty<string>
	{
		public StringPersistentProperty(string defaultValue, string key) : base(defaultValue, key)
		{
			Init();
		}

		protected override void Write(string value)
		{
			PlayerPrefs.SetString(Key, value);
			base.Write(value);
		}

		protected override string Read(string defaultValue)
		{
			return PlayerPrefs.GetString(Key, defaultValue);
		}
	}
}