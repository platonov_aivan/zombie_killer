﻿using System;
using UnityEngine;
namespace Game.Scripts.Gameplay.Model.Data.Properties.Persistent
{
    [Serializable]
    public class FloatPersistentProperty : PrefsPersistentProperty<float>
    {
        public FloatPersistentProperty(float defaultValue, string key) : base(defaultValue, key)
        {
            Init();
        }
        protected override void Write(float value)
        {
            PlayerPrefs.SetFloat(Key, value);
            base.Write(value);
        }
        protected override float Read(float defaultValue)
        {
            return PlayerPrefs.GetFloat(Key, defaultValue);
        }

    }
}