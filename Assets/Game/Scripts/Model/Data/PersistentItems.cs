﻿using System;
using System.Collections.Generic;
using System.Linq;
using Game.Scripts.Gameplay.Model.Data.Properties;
using Game.Scripts.Gameplay.Model.Data.Properties.Persistent;
using Game.Scripts.Gameplay.Model.Definitions.Player;
using UnityEngine;

namespace Game.Scripts.Gameplay.Model.Data
{
    [CreateAssetMenu(menuName = "Data/PersistentItems", fileName = "PersistentItems")]
    public class PersistentItems : ScriptableObject
    {
        [SerializeField] private IntPersistentProperty _gold;
        [SerializeField] private List<ImprovementProgress> _progress;

        public IntPersistentProperty Gold => _gold;
        public static PersistentItems I => _instance == null ? LoadPersistentItems() : _instance;
        
        private static PersistentItems _instance;

        public int GetImprovementLevel(StatId id)
        {
            foreach (var improvementProgress in _progress)
            {
                if (improvementProgress.Id == id)
                    return improvementProgress.Level.Value;
            }

            return 0;
        }

        public void ImprovementLevelUp(StatId id)
        {
            var progress = _progress.FirstOrDefault(x => x.Id == id);
            if (progress == null)
                _progress.Add(new ImprovementProgress(id, 1));
            else
                progress.Level.Value++;
            
        }

        public void DiscardLevels(StatId id)
        {
            var progress = _progress.FirstOrDefault(x => x.Id == id);
            if (progress == null)
                _progress.Add(new ImprovementProgress(id, 1));
            else
                progress.Level.Value = 0;
        }
        
        private void OnEnable()
        {
            _gold = new IntPersistentProperty(0, PersistentItemNames.Gold.ToString());
            
            foreach (var improvementProgress in _progress)
            {
                improvementProgress.Level = new IntPersistentProperty(0, improvementProgress.Id.ToString());
            }
        }

        private static PersistentItems LoadPersistentItems()
        {
            return _instance = Resources.Load<PersistentItems>("PersistentItems");
        }
        

        private void OnValidate()
        {
            Gold.Validate();
            foreach (var improvementProgress in _progress)
            {
                improvementProgress.Level.Validate();
            }
        }
        
        
    }
    
    public enum PersistentItemNames
    {
        Gold
    }
    
    [Serializable]
    public class ImprovementProgress
    {
        public StatId Id;
        public IntPersistentProperty Level;

        public ImprovementProgress(StatId id, int level)
        {
            Id = id;
            Level = new IntPersistentProperty(0, id.ToString());
        }
    }
}