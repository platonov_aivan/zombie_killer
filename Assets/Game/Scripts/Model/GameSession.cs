﻿using Game.Scripts.Gameplay.Model.Data;
using Game.Scripts.Gameplay.Model.Models;
using Game.Scripts.Utils;
using UnityEngine;

namespace Game.Scripts.Gameplay.Model
{
    public class GameSession : Singleton<GameSession>
    {
        [SerializeField] private string _heroId = "Hero";
        [SerializeField] private PlayerData _data;

        public PlayerData Data => _data;

        // public AssistantsModel AssistantsModel => _assistantsModel;
        public StatsModel StatsModel => _statsModel;

        // private AssistantsModel _assistantsModel;
        private StatsModel _statsModel;

        protected override void Awake()
        {
            base.Awake();
            InitModels();
            DontDestroyOnLoad(gameObject);
        }

        private void InitModels()
        {
            _data.InitPlayerData();
            // _assistantsData.InitAssistantData();

            // _assistantsModel = new AssistantsModel(_assistantsData);
            _statsModel = new StatsModel(_data);
        }

        // [ContextMenu("Add 500 coin")]
        // public void CoinCheat()
        // {
        //     _data.Coins.Value += 500;
        // }
        public void StoreState(object id)
        {
            throw new System.NotImplementedException();
        }

        public bool RestoreState(string id)
        {
            throw new System.NotImplementedException();
        }
    }
}