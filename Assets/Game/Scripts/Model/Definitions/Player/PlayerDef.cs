﻿using UnityEngine;

namespace Game.Scripts.Gameplay.Model.Definitions.Player
{
    [CreateAssetMenu(menuName = "Defs/PlayerDef", fileName = "PlayerDef")]
    public class PlayerDef : ScriptableObject
    {
        [SerializeField] private float _rageBuffDuration = 5f;
        [SerializeField] private StatDef[] _stats;
        [SerializeField] private int _maxHealthlh;


        public int MaxHealth => _maxHealthlh;
        public float RageBuffDuration => _rageBuffDuration;
        public StatDef[] Stats => _stats;

        public StatDef GetStat(StatId id)
        {
            foreach (var statDef in _stats)
            {
                if (statDef.ID == id)
                    return statDef;
            }

            return default;
        }
    }
}