﻿using System;
using UnityEngine;

namespace Game.Scripts.Gameplay.Model.Definitions.Player
{
    [Serializable]
    public struct StatDef
    {
        [SerializeField] private string _name;
        [SerializeField] private StatId _id;
        [SerializeField] private Sprite _icon;
        [SerializeField] private Sprite _iconDisabled;
        [SerializeField] private StatLevelDef[] _levels;

        public string Name => _name;
        public StatId ID => _id;
        public Sprite Icon => _icon;
        public Sprite IconDisabled => _iconDisabled;
        public StatLevelDef[] Levels => _levels;
    }

    [Serializable]
    public struct StatLevelDef
    {
        [SerializeField] private float _value;
        [SerializeField] private int _price;

        public float Value => _value;
        public int Price => _price;
    }

    public enum StatId
    {
        Capacity,
        Speed,
        Strength
    }
}