﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.Scripts.Gameplay.Model.Definitions
{
    [CreateAssetMenu(menuName = "Defs/LevelsDef", fileName = "LevelsDef")]
    public class LevelsDef : ScriptableObject
    {
        [SerializeField] private List<SceneField> _levels;

        public List<SceneField> Levels => _levels;

        public int LevelsCount => _levels.Count;


        public string[] GetLevelNameScenes()
        {
            return (from scene in _levels select scene.SceneName).ToArray();
        }

        [ContextMenu("Test")]
        private void Test()
        {
            foreach (var sceneName in GetLevelNameScenes())
            {
                Debug.Log(sceneName);
            }
        }
    }
}