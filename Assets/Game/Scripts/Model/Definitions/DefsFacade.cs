﻿using Game.Scripts.Gameplay.Model.Definitions.Player;
using UnityEngine;

namespace Game.Scripts.Gameplay.Model.Definitions
{
    [CreateAssetMenu(menuName = "Defs/DefsFacade", fileName = "DefsFacade")]
    public class DefsFacade : ScriptableObject
    {
        [SerializeField] private PlayerDef _player;
        [SerializeField] private LevelsDef _levelsDef;

        public static DefsFacade I => _instance == null ? LoadDefs() : _instance;
        
	    public PlayerDef Player => _player;

        public LevelsDef LevelsDef => _levelsDef;

        private static DefsFacade _instance;
        
        
        
        private static DefsFacade LoadDefs()
        {
            return _instance = Resources.Load<DefsFacade>("DefsFacade");
        }
    }
}