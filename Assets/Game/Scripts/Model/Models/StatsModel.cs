﻿using System;
using Game.Scripts.Gameplay.Model.Data;
using Game.Scripts.Gameplay.Model.Definitions;
using Game.Scripts.Gameplay.Model.Definitions.Player;
using Game.Scripts.Utils.Disposables;
using UnityEngine;

namespace Game.Scripts.Gameplay.Model.Models
{
    public class StatsModel
    {
        public event Action OnChanged;
        public event Action<StatId> OnUpgraded;
        
        private readonly PlayerData _data;
        
        public StatsModel(PlayerData data)
        {
            _data = data;
        }

        public IDisposable SubscribeOnChanged(Action call)
        {
            OnChanged += call;
            return new ActionDisposable(() => OnChanged -= call);
        }
        public IDisposable SubscribeOnUpgraded(Action<StatId> call)
        {
            OnUpgraded += call;
            return new ActionDisposable(() => OnUpgraded -= call);
        }
        
        public bool LevelUp(StatId id)
        {
            var def = DefsFacade.I.Player.GetStat(id);
            var nextLevel = GetCurrentLevel(id) + 1;

            if (def.Levels.Length <= nextLevel) return false;

            var price = def.Levels[nextLevel].Price;
            if (PersistentItems.I.Gold.Value < price) return false;

            PersistentItems.I.Gold.Value -= price;
            PersistentItems.I.ImprovementLevelUp(id);
            
            OnChanged?.Invoke();
            OnUpgraded?.Invoke(id);
            return true;
        }

        public void SellAllStats()
        {
            foreach (StatId statId in Enum.GetValues(typeof(StatId)))
            {
                SellStat(statId);
            }
        }

        public void SellStat(StatId statId)
        {
            var def = DefsFacade.I.Player.GetStat(statId);
            for (var i = GetCurrentLevel(statId); i > 0; i--)
            {
                PersistentItems.I.Gold.Value += def.Levels[i].Price;
            }
            PersistentItems.I.DiscardLevels(statId);
            
            OnUpgraded?.Invoke(statId);
        }

        public float GetValue(StatId id, int level = -1)
        {
            return GetLevelDef(id, level).Value;
        }

        public StatLevelDef GetLevelDef(StatId id, int level = -1)
        {
            if (level == -1) level = GetCurrentLevel(id);
            var def = DefsFacade.I.Player.GetStat(id);
            if (def.Levels.Length > level)
                return def.Levels[level];
            
            return default;
        }



        public int GetCurrentLevel(StatId id) => PersistentItems.I.GetImprovementLevel(id);
        
    }
}