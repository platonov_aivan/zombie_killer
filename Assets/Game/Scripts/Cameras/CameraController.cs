﻿
using Game.Scripts.Utils;
using UnityEngine;
/*
namespace Game.Scripts.Gameplay.Cameras
{ public class CameraController : Singleton<CameraController>
    {
        [SerializeField] private Camera _mainCamera;
       // [SerializeField] private CinemachineVirtualCamera _cameraFirst;
       // [SerializeField] private CinemachineVirtualCamera _cameraSecond;
        [SerializeField] private Animator _animator;

        public Camera MainCamera => _mainCamera;
        public Transform MainCameraTransform => _cachedMainCameraTransform == null
            ? _cachedMainCameraTransform = _mainCamera.transform
            : _cachedMainCameraTransform;
        private Transform _cachedMainCameraTransform;


        private static readonly int NextCameraKey = Animator.StringToHash("next-camera");

        

        public float DefaultBlend { get; private set;}
        

        public void SetBlend(float value)
        {
            _cinemachineStateDriven.m_DefaultBlend.m_Time = value;
        }
        
        public void SetDefaultBlend()
        {
            _cinemachineStateDriven.m_DefaultBlend.m_Time = DefaultBlend;
        }

        public void CameraMoveTo(Transform cameraTransform)
        {
            if (_currentCamera == _cameraFirst)
            {
                SetCamera(_cameraSecond, cameraTransform);
            }
            else if (_currentCamera == _cameraSecond)
            {
                SetCamera(_cameraFirst, cameraTransform);
            }
            
            _animator.SetTrigger(NextCameraKey);
        }

        private void SetCamera(CinemachineVirtualCamera assignCamera, Transform newTransform)
        {
            _currentCamera = assignCamera;
            assignCamera.transform.position = newTransform.position;
            assignCamera.transform.rotation = newTransform.rotation;
        }

        private void Start()
        {
            _currentCamera = _cameraFirst;
        }

        protected override void Awake()
        {
            base.Awake();
            DefaultBlend = _cinemachineStateDriven.m_DefaultBlend.m_Time;
            DontDestroyOnLoad(gameObject);
        }
    }
}*/