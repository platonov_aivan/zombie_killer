﻿using System;
using System.Collections.Generic;
using System.Linq;
using Game.Scripts.Utils;
using UnityEngine;
using UnityEngine.Events;

namespace Game.Scripts.CheckCircle
{
    public class CheckCircleOverlap : MonoBehaviour

    {
        [SerializeField] private float _radius = 1f;
        [SerializeField] private LayerMask _mask;
        [SerializeField] private string[] _tags;
        private readonly Collider[] _interactionResult = new Collider[10];
       
#if UNITY_EDITOR
        private void OnDrawGizmosSelected()
        {
            var defaultColor = Gizmos.color;
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, _radius);
            Gizmos.color = defaultColor;
        }
#endif
        public Collider[] Check()
        {
            var size = Physics.OverlapSphereNonAlloc(
                transform.position,
                _radius,
                _interactionResult,
                _mask);
            return _interactionResult.ToArray();
        }
    }
}