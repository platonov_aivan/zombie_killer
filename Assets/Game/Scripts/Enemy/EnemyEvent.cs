﻿using UnityEngine;

namespace Game.Scripts.Enemy
{
    public class EnemyEvent : MonoBehaviour
    {
        [SerializeField] private EnemyController _enemy;

        public void OnAttack()
        {
            _enemy.OnAttack();
        }

        public void OnAttacked()
        {
            _enemy.OnAttacked();
        }
        public void OnAttackedEnd()
        {
            _enemy.OnAttackedEnd();
        }
        public void OnWalking()
        {
            _enemy.OnWalking();
        }
        public void OnWalkingEnd()
        {
            _enemy.OnWalkingEnd();
        }
        
        
    }
}