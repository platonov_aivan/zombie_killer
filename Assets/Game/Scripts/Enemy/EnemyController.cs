﻿using System;
using Game.Scripts.CheckCircle;
using Game.Scripts.Colliders.Triggers;
using Game.Scripts.Components.Health;
using Game.Scripts.Gameplay.Colliders.Triggers;
using Game.Scripts.Utils;
using Game.Scripts.Utils.Disposables;
using UnityEngine;
using UnityEngine.AI;

namespace Game.Scripts.Enemy
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private EnemyController _enemyController;
        [SerializeField] private Transform _enemyTransform;
        [SerializeField] private NavMeshAgent _agent;
       // [SerializeField] private EnterTrigger _vision;
        [SerializeField] private Animator _enemyAnimator;
        [SerializeField] private float _distanceToPlayer;
        [SerializeField] private HealthComponent _hp;
        [SerializeField] public CheckCircleOverlap _attackRange;
        [SerializeField] private int _damage;
        [SerializeField] private float _delaySave = 1.5f;


        private float _lastSave;
        
        private static readonly int Walk = Animator.StringToHash("walk");
        private static readonly int Attack = Animator.StringToHash("attack");


        private CompositeDisposable _trash = new CompositeDisposable();
        private Quaternion _updatedRotation;
        private float _startRotationSpeed = 3f;
        private bool _animationAttackEnd = false;
        private bool _enemyAttacked;
        private bool _enemyWalking;
        private bool _enemyInAttack = false;
        private bool _enemyInWalk;

        public bool IsALive { get; private set; } = true;


        private void Awake()
        {
          //  _vision.OnInteract += VisionToFollow;
            _trash.Retain(_hp.SubscribeOnDie(OnDie));
        }

        private void OnDie()
        {
            _agent.isStopped = true;
            _agent.enabled = false;
            _enemyController.enabled = false;
            Locator.Instance.Player.ZoombieALive = false;
            IsALive = false;
        }

       /* private void VisionToFollow()
        {
            if (!_vision)
            {
                _agent.SetDestination(Locator.Instance.PlayerPelvis.position);
            }
        }*/

        private void Update()
        {
            _distanceToPlayer = Vector3.Distance(Locator.Instance.PlayerPelvis.position, _enemyTransform.transform.position);
            if (_distanceToPlayer < 1.5f && _distanceToPlayer > 0.5f)
            {
                _agent.isStopped = true;
                _enemyInWalk = false;
                _enemyInAttack = true;
                _enemyWalking = false;
                EnemyAttackTrigger();
                Vector3 target = (_enemyTransform.position - Locator.Instance.PlayerPelvis.position) * -1f;
                Vector3 toTarget = new Vector3(target.x, 0f, target.z);
                _updatedRotation = Quaternion.Euler(0f, Quaternion.LookRotation(toTarget).eulerAngles.y, 0f);
                Quaternion rotation = Quaternion.Slerp(transform.rotation, _updatedRotation, _startRotationSpeed * Time.fixedDeltaTime);
                transform.rotation = rotation;
            }

            if (_distanceToPlayer > 2f && _distanceToPlayer < 20f)
            {
                Locator.Instance.Player.ZoombieALive = true;
                _agent.isStopped = false;
                _enemyInWalk = true;
                _enemyInAttack = false;
                _enemyAttacked = false;
                EnemyWalkTrigger();
                if (Time.time > _lastSave + _delaySave)
                {
                    _agent.SetDestination(Locator.Instance.PlayerPelvis.position);
                    _lastSave = Time.time;
                }
            }
        }

        private void EnemyAttackTrigger()
        {
            if (_enemyInAttack == false) return;
            if (_enemyInAttack == true && _enemyInWalk == false && _enemyAttacked == false)
            {
                _enemyAnimator.ResetTrigger(Walk);
                _enemyAnimator.SetTrigger(Attack);
                _enemyInAttack = false;
                
            }
            
        }

        private void EnemyWalkTrigger()
        {
            if (_enemyInWalk == false) return;
            if (_enemyInWalk == true && _enemyInAttack == false && _enemyWalking == false)
            {
                _enemyAnimator.ResetTrigger(Attack);
                _enemyAnimator.SetTrigger(Walk);
                _enemyInWalk = false;
                
            }
        }


        private void OnDestroy()
        {
          //  _vision.OnInteract -= VisionToFollow;
            _trash.Dispose();
        }

        public void OnAttack()
        {
            var colliderArray = _attackRange.Check();
            foreach (var collider in colliderArray)
            {
                if (collider == null) return;
                var hpHero = collider.GetComponent<HealthComponent>();
                hpHero?.ChangeHealth(_damage);
            }
        }

        public void OnAttacked()
        {
            _enemyAttacked = true;
        }

        public void OnAttackedEnd()
        {
            _enemyAttacked = false;
        }
        
        public void OnWalking()
        {
            _enemyWalking = true;
        }
        
        public void OnWalkingEnd()
        {
            _enemyWalking = false;
        }

        
    }
}