﻿using System;
using UnityEngine;
namespace Game.Scripts.Utils
{
	[Serializable]
	public class Module<TTargetType>
	{
		protected TTargetType Target;


		public virtual void Init(TTargetType target)
		{
			Target = target;
		}
		

		public virtual void Update()
		{

		}

		public virtual void FixedUpdate()
		{

		}

		public virtual void DestroyModule()
		{
			
		}
		
	}
}