﻿using UnityEngine;
namespace Game.Scripts.Utils
{
    public class WiggleAndScrollComponent : MonoBehaviour
    {
        [SerializeField] private bool _wiggleOnOff = true;
        [SerializeField] private bool _scrollOnOff = true;
        [Header("Wiggle")] 
        [SerializeField] private bool _xAxisWiggle;
        [SerializeField] private bool _yAxisWiggle = true;
        [SerializeField] private bool _zAxisWiggle;
        [SerializeField] private float _speedWiggle = 6f;
        [SerializeField] private float _amplitudeWiggle = 0.3f;
        [Header("Scroll")]
        [SerializeField] private bool _xAxisScroll;
        [SerializeField] private bool _yAxisScroll = true;
        [SerializeField] private bool _zAxisScroll;
        [SerializeField] private float _speedScroll = 50f;
        [Header("Randomize")]
        [SerializeField] private bool _randomizeWiggleOnStart;
        [SerializeField] private bool _randomizeScrollOnStart;
        
        

        
        private Transform CachedTransform => _cachedTransform == null ? _cachedTransform = transform : _cachedTransform;
        private Transform _cachedTransform;

        private float CurrentTime => Time.time - _timeStart;
        private float _timeStart;
        
        
        private Vector3 _updatedRotation;
        private Vector3 _updatedLocalPosition;


        private void Start()
        {
            _updatedRotation = CachedTransform.rotation.eulerAngles;
            _updatedLocalPosition = CachedTransform.localPosition;
            _timeStart = Time.time + (_randomizeWiggleOnStart ? Random.value * 10 : 0f);

            if (_randomizeScrollOnStart)
            {
                if (_xAxisScroll)
                    _updatedRotation.x = Random.Range(0, 359);
                if(_yAxisScroll)
                    _updatedRotation.y = Random.Range(0, 359);
                if(_zAxisScroll)
                    _updatedRotation.z = Random.Range(0, 359);
            }
        }

        private void Update()
        {
            if (_wiggleOnOff)
            {
                UpdatePosition(ref _updatedLocalPosition);
                CachedTransform.localPosition = _updatedLocalPosition;
            }
            if (_scrollOnOff)
            {
                UpdateRotation(ref _updatedRotation);
                CachedTransform.rotation = Quaternion.Euler(_updatedRotation);
            }
        }

        private void UpdatePosition(ref Vector3 pos)
        {
            if (_xAxisWiggle)
                UpdateAxis(ref pos.x);
            if (_yAxisWiggle)
                UpdateAxis(ref pos.y);
            if (_zAxisWiggle)
                UpdateAxis(ref pos.z);
            
        }

        private void UpdateAxis(ref float axisPos)
        {
            axisPos = Mathf.Sin(CurrentTime * _speedWiggle) * _amplitudeWiggle;
        }


        private void UpdateRotation(ref Vector3 rotate)
        {
            if(_xAxisScroll)
                UpdateAngle(ref rotate.x);
            if(_yAxisScroll)
                UpdateAngle(ref rotate.y);
            if(_zAxisScroll)
                UpdateAngle(ref rotate.z);
        }
        
        private void UpdateAngle(ref float angle)
        {
            angle += _speedScroll * Time.deltaTime;

            if (angle > 360)
                angle -= 360;
            if(angle < -360)
                angle += 360;
        }
    }
}