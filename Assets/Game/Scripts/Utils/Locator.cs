﻿
using Game.Scripts.Player;
using UnityEngine;

namespace Game.Scripts.Utils
{
    public class Locator : Singleton<Locator>
    {
       /* [SerializeField] private PlayerInput _playerInput;
        

        public PlayerInput PlayerInput => _playerInput;*/

       [SerializeField] private Transform _playerPelvis;
       [SerializeField] private PlayerController _player;

       public PlayerController Player => _player;

       public Transform PlayerPelvis => _playerPelvis;
       //  private HUD _hud;

       // public static HUD Hud => Instance?._hud;
       public static GameObject MainPool
        {
            get
            {
                if (_mainPool != null) return _mainPool;
                _mainPool = GameObject.Find("###_MAIN_POOL_###");
                return _mainPool;
            }
        }
       /* public static Level Level
        {
            get
            {
                if (_level != null) return _level;
                _level = FindObjectOfType<Level>();
                return _level;
            }
        }

        private static Level _level;*/
        private static GameObject _mainPool;


        protected override void Awake()
        {
            base.Awake();
            DontDestroyOnLoad(gameObject);
        }
    }
    
}