using System.Collections;
using UnityEngine;

namespace Game.Scripts.Utils
{
    public class DisableAfterDelay : MonoBehaviour
    {
        [SerializeField] private float _disableDelay = 2f;

        private Coroutine _disableCoroutine;
        
        private void OnEnable()
        {
            if(_disableCoroutine != null)
                StopCoroutine(_disableCoroutine);
            _disableCoroutine = StartCoroutine(DisableCoroutine());
        }

        private IEnumerator DisableCoroutine()
        {
            yield return new WaitForSeconds(_disableDelay);

            gameObject.SetActive(false);
        }
    }
}