﻿using System;
using System.Collections;
using System.Collections.Generic;


using Game.Scripts.Gameplay.Model;
using Game.Scripts.Gameplay.Model.Definitions;

using Game.Scripts.Utils;
using Game.Scripts.Utils.Disposables;
using UnityEngine;
/*
namespace Game.Scripts
{
    public class Level : MonoBehaviour
    {
        [SerializeField] private HeroController _hero;
        [SerializeField] private Camera _viewCamera;
        [SerializeField] private Transform _cameraSpanStart;
        [SerializeField] private Transform _cameraSpanEnd;
        [SerializeField] private Transform _cameraStartPos;
        [SerializeField] private GameObject _enemiesContainer;
        [SerializeField] private float _flyingTime = 4f;
        [SerializeField]private float _winDelay = 3f;
        [SerializeField]private float _loseDelay = 2f;

        private List<EnemyController> _enemies;

        public HeroController Hero => _hero;

        public Camera ViewCamera => _viewCamera;
        
        private Transform _cachedHeroTransform;

        private readonly CompositeDisposable _trash = new CompositeDisposable();
        

        private void Start()
        {
            ViewCamera.depth = -10f;
            InputManager.Instance.Locker.Retain(this);
            StartCoroutine(StartLevelCoroutine());
            _trash.Retain(_hero.Health.SubscribeOnDie(OnHeroDie)); 
        }

        private void OnHeroDie()
        {
            LoseLevel();
        }

        

        private IEnumerator StartLevelCoroutine()
        {
            yield return null;
            
            CameraController.Instance.SetBlend(0f);
            CameraController.Instance.CameraMoveTo(_cameraSpanStart);
            yield return null;
            
            CameraController.Instance.SetBlend(_flyingTime);
            CameraController.Instance.CameraMoveTo(_cameraSpanEnd);
            yield return new WaitForSeconds(_flyingTime);
              
            CameraController.Instance.SetDefaultBlend();
            CameraController.Instance.CameraMoveTo(_cameraStartPos);
            yield return new WaitForSeconds(CameraController.Instance.DefaultBlend);

            StartLifeBarHero();
            StartGameProgress();
            yield return null;
        }

        private void StartLifeBarHero()
        {
            Locator.Hud.OpenLifeBarHero();
            InputManager.Instance.Locker.Release(this);
        }

        private void StartGameProgress()
        {
            InputManager.Instance.Locker.Release(this);
            _enemies = new List<EnemyController>(_enemiesContainer.GetComponentsInChildren<EnemyController>());
            foreach (var enemyController in _enemies)
            {
                _trash.Retain(enemyController.Hp.SubscribeOnDie(OnEnemyDie));
            }
        }

        private void OnEnemyDie()
        {
            _enemies.RemoveAt(0);
            if (_enemies.Count <= 0)
            {
                WinLevel();
            }
        }

        private void WinLevel()
        {
            StartCoroutine(WinCoroutine());
        }

        private IEnumerator WinCoroutine()
        {
            InputManager.Instance.Locker.Retain(this);
            CameraController.Instance.CameraMoveTo(Hero.CameraWinLevel);
            _hero.WinDance();
            if (GameSession.Instance.Data.ProgressSave.Value < DefsFacade.I.LevelsDef.LevelsCount -1)
            {
                GameSession.Instance.Data.ProgressSave.Value++;
            }
            yield return new WaitForSeconds(_winDelay);

            Locator.Hud.OpenWinWindow();
        }

        private void LoseLevel()
        {
            StartCoroutine(LoseCoroutine());
        }

        private IEnumerator LoseCoroutine()
        {
            InputManager.Instance.Locker.Retain(this);
            yield return new WaitForSeconds(_loseDelay);
            Locator.Hud.OpenLoseWindow();
        }

        private void OnDestroy()
        {
            _trash.Dispose();
        }

        private void OnDisable()
        {
            InputManager.Instance.Locker.Release(this);
        }
    }
}*/