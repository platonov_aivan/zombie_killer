﻿/*
using Game.Scripts.Gameplay.Model;
using Game.Scripts.Gameplay.Model.Definitions;



using Game.Scripts.Utils.Disposables;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Game.Scripts
{
    public class HUD : MonoBehaviour
    {
        
        [SerializeField] private Button _nextLevel;
        [SerializeField] private Button _mainMenu;
        [SerializeField] private Button _retryLevel;
        [SerializeField] private CanvasGroup _canvasGroup;
        [SerializeField] private CanvasGroup _canvasGroupLifeBar;
        [SerializeField] private TextMeshProUGUI _text;


        private CompositeDisposable _trash = new CompositeDisposable();
        

        public  virtual bool Enable
        {
            set
            {
                if (value)
                {
                    _canvasGroup.alpha = 1f;
                    _canvasGroup.interactable = true;
                    _canvasGroup.blocksRaycasts = true;
                }
                else
                {
                    _canvasGroup.alpha = 0f;
                    _canvasGroup.interactable = false;
                    _canvasGroup.blocksRaycasts = false;
                }
            }
        }
        public  virtual bool EnableLifeBar
        {
            set
            {
                if (value)
                {
                    _canvasGroupLifeBar.alpha = 1f;
                    _canvasGroupLifeBar.interactable = true;
                    _canvasGroupLifeBar.blocksRaycasts = true;
                }
                else
                {
                    _canvasGroupLifeBar.alpha = 0f;
                    _canvasGroupLifeBar.interactable = false;
                    _canvasGroupLifeBar.blocksRaycasts = false;
                }
            }
        }
        

        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
            _nextLevel.onClick.AddListener(OnNextLevel);
            _mainMenu.onClick.AddListener(OnMainMenu);
            _retryLevel.onClick.AddListener(OnRetryLevel);
            Enable = false;
        }

        private void OnRetryLevel()
        {
            Enable = false;
            SceneLoader.Instance.LoadScene(DefsFacade.I.LevelsDef.Levels[GameSession.Instance.Data.CurrentLevel.Value].SceneName);
        }

        private void OnMainMenu()
        {
            Enable = false;
            SceneLoader.Instance.LoadScene("MainMenu");
        }

        private void OnNextLevel()
        {
            Enable = false;
            GameSession.Instance.Data.CurrentLevel.Value++;
            SceneLoader.Instance.LoadScene(DefsFacade.I.LevelsDef.Levels[GameSession.Instance.Data.CurrentLevel.Value].SceneName);
        }

        public void OpenWinWindow()
        {
            Enable = true;
            _text.text = "YOU WIN!";
            _retryLevel.gameObject.SetActive(false);
            _nextLevel.gameObject.SetActive(true);
            if (GameSession.Instance.Data.CurrentLevel.Value >= DefsFacade.I.LevelsDef.LevelsCount - 1)
            {
                _nextLevel.gameObject.SetActive(false);
            }
            EnableLifeBar = false;
        }

        public void OpenLoseWindow()
        {
            Enable = true;
            _text.text = "YOU LOSE!";
            _nextLevel.gameObject.SetActive(false);
            _retryLevel.gameObject.SetActive(true);
            EnableLifeBar = false;
        }

        public void OpenLifeBarHero()
        {
            EnableLifeBar = true;
        }

        private void OnDestroy()
        {
            _nextLevel.onClick.RemoveListener(OnNextLevel);
            _mainMenu.onClick.RemoveListener(OnMainMenu);
            _retryLevel.onClick.RemoveListener(OnRetryLevel);
            _trash.Dispose();
        }
    }
}*/