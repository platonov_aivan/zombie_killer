﻿using System;
using System.Collections;
using UnityEngine;

namespace Game.Scripts.Utils.ObjectPool
{
    [RequireComponent(typeof(PoolItem))]
    public class ReleaseAfterDelay : MonoBehaviour
    {
        [SerializeField] private float _delay = 3f;

        private Coroutine _coroutine;
        private PoolItem _poolItem;

        private void Start()
        {
            _poolItem = GetComponent<PoolItem>();
            _poolItem.OnRestart += RestartObject;
            
            RestartObject();
        }

        private void RestartObject()
        {
            if(_coroutine != null)
                StopCoroutine(_coroutine);
            _coroutine = StartCoroutine(ReleaseAfterTimeCoroutine());
        }

        private IEnumerator ReleaseAfterTimeCoroutine()
        {
            yield return new WaitForSeconds(_delay);
            _poolItem.Release();
        }

        private void OnDestroy()
        {
            _poolItem.OnRestart -= RestartObject;
        }
    }
}