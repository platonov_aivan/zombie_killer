﻿using System.Collections.Generic;
using UnityEngine;

namespace Game.Scripts.Utils.ObjectPool
{
    public class Pool : MonoBehaviour
    {
        public static GameObject PoolGameObject { get; private set; }
        private static Pool _instance;

        private readonly Dictionary<int, Queue<PoolItem>> _items = new Dictionary<int, Queue<PoolItem>>();

        private Transform CachedTransform => _cachedTransform == null ? _cachedTransform = transform : _cachedTransform;
        private Transform _cachedTransform;
        
        public static Pool Instance
        {
            get
            {
                if (_instance == null)
                {
                    var go = new GameObject("###_MAIN_POOL_###");
                    _instance = go.AddComponent<Pool>();
                    PoolGameObject = go;
                }
                return _instance;
            }
        }

        public GameObject Get(GameObject go, Vector3 position)
        {
            var id = go.GetInstanceID();
            var queue = RequireQueue(id);

            if (queue.Count > 0)
            {
                var pooledItem = queue.Dequeue();
                var pooledItemTransform = pooledItem.transform;
                
                pooledItemTransform.position = position;
                //pooledItemTransform.localScale = Vector3.one;
                pooledItem.gameObject.SetActive(true);
                pooledItem.Restart();
                
                return pooledItem.gameObject;
            }

            var instance = SpawnUtils.Spawn(go, position, gameObject.name);
            //instance.transform.localScale = Vector3.one;
            instance.SetActive(true);
            
            var poolItem = instance.GetComponent<PoolItem>();
            poolItem.Retain(id, this);

            return instance;
        }
        
        private Queue<PoolItem> RequireQueue(int id)
        {
            if (!_items.TryGetValue(id, out var queue))
            {
                queue = new Queue<PoolItem>();
                _items.Add(id, queue);
            }

            return queue;
        }
        public void Release(int id, PoolItem poolItem)
        {
            var queue = RequireQueue(id);
            queue.Enqueue(poolItem);

            poolItem.transform.parent = CachedTransform;
            poolItem.gameObject.SetActive(false);
        }
    }
}