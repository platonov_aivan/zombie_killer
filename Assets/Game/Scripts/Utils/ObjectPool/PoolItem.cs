﻿using System;
using Game.Scripts.Utils.Disposables;
using UnityEngine;

namespace Game.Scripts.Utils.ObjectPool
{
    public class PoolItem : MonoBehaviour
    {
        public event Action OnRestart;
        public event Action OnRelease;
        
        private int _id;
        private Pool _pool;
        
        public void Restart()
        {
            OnRestart?.Invoke();
        }
        
        public void Release()
        {
            OnRelease?.Invoke();
            _pool.Release(_id, this);
        }

        public void Retain(int id, Pool pool)
        {
            _id = id;
            _pool = pool;
        }

        public IDisposable SubscribeOnRestart(Action call)
        {
            OnRestart += call;
            return new ActionDisposable(() => OnRestart -= call);
        }
        public IDisposable SubscribeOnRelease(Action call)
        {
            OnRelease += call;
            return new ActionDisposable(() => OnRelease -= call);
        }
    }
}