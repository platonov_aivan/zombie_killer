﻿namespace Game.Scripts.Utils.StateMachineBase
{
	public abstract class State<TTargetType>
	{
		protected StateMachine<TTargetType> StateMachine;

		protected TTargetType Target;

		protected State(TTargetType target, StateMachine<TTargetType> stateMachine)
		{
			Target = target;
			StateMachine = stateMachine;
		}

		public virtual void Init(TTargetType target, StateMachine<TTargetType> stateMachine)
		{
			Target = target;
			StateMachine = stateMachine;
		}

		public virtual void Enter()
		{
			
		}

		public virtual void HandleInput()
		{

		}

		public virtual void LogicUpdate()
		{

		}

		public virtual void PhysicsUpdate()
		{

		}

		public virtual void Exit()
		{

		}
	}
}