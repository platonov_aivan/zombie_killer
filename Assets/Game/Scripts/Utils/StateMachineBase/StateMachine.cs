﻿namespace Game.Scripts.Utils.StateMachineBase
{
	public class StateMachine<TTargetType>
	{
		public State<TTargetType> CurrentState { get; private set; }
		
		
		public void Initialize(State<TTargetType> startingState)
		{
			CurrentState = startingState;
			startingState.Enter();
		}

		public void ChangeState(State<TTargetType> newState)
		{
			CurrentState.Exit();

			CurrentState = newState;
			newState.Enter();
		}
	}
}